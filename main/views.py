from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
from .forms import TextForm
from .enkriptor.processor import *

def home(request):
    final_text = ""
    if request.method == 'POST':
        form = TextForm(request.POST)
        if form.is_valid():
            keyword = form.cleaned_data['keyword']
            text = form.cleaned_data['text']
            option = form.cleaned_data['option']

            if option == "encode":
                try:
                    final_text = encryption.perform(keyword, text)
                    print(type(final_text))
                except:
                    final_text = "Please use only supported symbols a b c d e f g h i j k l m n o p r s t u v x y w z . ,"

            else:
                try:
                    final_text = decryption.perform(keyword, text)
                except:
                    final_text = "Please use only supported symbols a b c d e f g h i j k l m n o p r s t u v x y w z . ,"


        #print(text, option)

    form = TextForm()
    
    return render(request, 'main/user_interface.html', {'form': form, 'text':final_text})
