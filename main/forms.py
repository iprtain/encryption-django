from django import forms


class TextForm(forms.Form):
    keyword = forms.CharField()
    text = forms.CharField(widget=forms.Textarea)
    option = forms.ChoiceField(choices=[('encode','Encode'), ('decode', 'Decode')])